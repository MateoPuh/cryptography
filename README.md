# Assignment: Cryptography 

Create a software system with GUI for creating and reading a digital envelope and creating and verifying a digital signature and digital stamp using ready-made, freely available cryptographic algorithms. From cryptographic algorithms, the user should be able to choose all the listed algorithms from each category:

- Symmetric algorithm: AES and 3-DES (with a choice of all possible key sizes for each algorithm and at least two encryption methods (ECB, CBC, OFB, CFB, CTR))
- Asymmetric algorithm: RSA (with several different key sizes)
- Message Hash function: SHA-2 or SHA-3. Enable selection between at least two of the four versions of the algorithm, e.g. SHA3-256 and SHA3-512)

Keys, envelopes, signatures and stamps should be in following format:

```
[...]
---BEGIN OS2 CRYPTO DATA---
[... contents ...]
---END OS2 CRYPTO DATA---
[...]
```

Content should be in following format:

```
Field:
    Value
```

Possible `Field` values:

- Description: - Secret key, Public key, Private key, Signature, Envelope, Crypted file [...]
- File name: - (optional)
- Method: - algorithm: DES, RSA, AES, SHA-1, [...] (if there are multiple algorithms each is listed in its own row)
- Key length: - (HEX)
- Secret key: - for symmetric algorithms (HEX)
- Initialization vector:
- Modulus: - `n` in RSA (HEX)
- Public exponent: - `e` in RSA (part of public kye) (HEX)
- Private exponent: - `d` in RSA (part of private key) (HEX)
- Signature: - Message or envelope hash (HEX)
- Data: - crypted file in base64 (not HEX)
- Envelope data: - message crypted with symmetric key in base64 (not HEX)
- Envelope crypt key: - symmetric key crypted with public key of receiver (HEX)

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Content {

    private static String CRYPTO_DATA_BEGIN = "---BEGIN OS2 CRYPTO DATA---";
    private static String CRYPTO_DATA_END = "---END OS2 CRYPTO DATA---";
    private static String DESCRIPTION = "Description:";
    private static String FILENAME = "File name:";
    private static String METHOD = "Method:";
    private static String KEY_LENGTH = "Key length:";
    private static String SECRET_KEY = "Secret key:";
    private static String INIT_VECTOR = "Initialization vector:";
    private static String MODULUS = "Modulus:";
    private static String PUBLIC_EXPONENT = "Public exponent:";
    private static String PRIVATE_EXPONENT = "Private exponent:";
    private static String SIGNATURE = "Signature:";
    private static String DATA = "Data:";
    private static String ENVELOPE_DATA = "Envelope data:";
    private static String ENVELOPE_CRYPT_KEY = "Envelope crypt key:";

    private static Map<String, Strategy> contentMap = new HashMap<>() {{
        put(DESCRIPTION, (Content::setDescription));
        put(FILENAME, (Content::setFilename));
        put(SECRET_KEY, (Content::setSecretKey));
        put(INIT_VECTOR, (Content::setInitVector));
        put(MODULUS, (Content::setModulus));
        put(PUBLIC_EXPONENT, (Content::setPublicExponent));
        put(PRIVATE_EXPONENT, (Content::setPrivateExponent));
        put(SIGNATURE, (Content::setSignature));
        put(DATA, (Content::setData));
        put(ENVELOPE_DATA, (Content::setEnvelopeData));
        put(ENVELOPE_CRYPT_KEY, (Content::setEnvelopeCryptKey));
    }};

    private String filePath;
    private String description;
    private String filename;
    private Encryption.EncryptionMethod[] method;
    private String[] keyLength;
    private String secretKey;
    private String initVector;
    private String modulus;
    private String publicExponent;
    private String privateExponent;
    private String signature;
    private String data;
    private String envelopeData;
    private String envelopeCryptKey;

    public Content() {
    }

    public Content(String description, String filename, Encryption.EncryptionMethod[] method, String[] keyLength, String secretKey, String initVector, String modulus, String publicExponent, String privateExponent, String signature, String data, String envelopeData, String envelopeCryptKey) {
        this.description = description;
        this.filename = filename;
        this.method = method;
        this.keyLength = keyLength;
        this.secretKey = secretKey;
        this.initVector = initVector;
        this.modulus = modulus;
        this.publicExponent = publicExponent;
        this.privateExponent = privateExponent;
        this.signature = signature;
        this.data = data;
        this.envelopeData = envelopeData;
        this.envelopeCryptKey = envelopeCryptKey;
    }

    public static Content fromFile(String filename) throws IOException {
        Content content = new Content();
        List<String> allLines = Files.readAllLines(Paths.get(filename));

        content.setFilePath(filename);

        int i = 0;

        // READ BEGIN LINE
        while (i < allLines.size()) {
            if (allLines.get(i).trim().equals(CRYPTO_DATA_BEGIN)) {
                i++;
                break;
            }
            i++;
        }

        // IF THERE IS NO BEGIN LINE THROW EXCEPTION
        if (i == allLines.size()) {
            throw new RuntimeException("Wrong file format.");
        }

        // READ CONTENT OF FILE
        while (i < allLines.size()) {
            // READ FIELD NAME
            String field = allLines.get(i).trim();

            // IF LINE IS EMPTY SKIP
            if (field.isEmpty()) {
                i++;
                continue;
            }

            // IF END LINE IS READ RETURN
            if (field.trim().equals(CRYPTO_DATA_END)) {
                return content;
            }

            // READ VALUE

            // IF FIELD IS IN CONTENT MAP
            if (contentMap.containsKey(field)) {
                StringBuilder value = new StringBuilder();

                while (allLines.get(i + 1).startsWith("    ")) {
                    value.append(allLines.get(i + 1).trim());
                    i++;
                }

                contentMap.get(field).setContent(content, value.toString());
                // IF FIELD IS METHOD
            } else if (field.equals(METHOD)) {
                List<Encryption.EncryptionMethod> methods = new ArrayList<>();

                while (allLines.get(i + 1).startsWith("    ")) {
                    Optional<Encryption.EncryptionMethod> m = Encryption.EncryptionMethod.fromName(allLines.get(i + 1).trim());

                    if (m.isEmpty()) {
                        throw new RuntimeException("Unknown method.");
                    }

                    methods.add(m.get());
                    i++;
                }

                Encryption.EncryptionMethod[] methodArray = new Encryption.EncryptionMethod[methods.size()];
                for (int j = 0; j < methods.size(); j++) {
                    methodArray[j] = methods.get(j);
                }

                content.setMethod(methodArray);
                // IF FIELD IS KEY LENGTH
            } else if (field.equals(KEY_LENGTH)) {
                List<String> keyLengths = new ArrayList<>();

                while (allLines.get(i + 1).startsWith("    ")) {
                    keyLengths.add(allLines.get(i + 1).trim());
                    i++;
                }

                String[] keyLengthsArray = new String[keyLengths.size()];

                for (int ki = 0; ki < keyLengths.size(); ki++) {
                    keyLengthsArray[ki] = keyLengths.get(ki);
                }

                content.setKeyLength(keyLengthsArray);
            }

            i++;
        }

        // THROW EXCEPTION IF THERE IS NO END LINE
        throw new RuntimeException("Wrong file format.");
    }

    public boolean checkValidity() {
        if (description == null || method == null) {
            return false;
        }

        return true;
    }

    public void toFile(String outputFile) {
        try {
            FileWriter fileWriter = new FileWriter(outputFile);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(CRYPTO_DATA_BEGIN);

            if (description != null) {
                printWriter.println(DESCRIPTION);
                printWriter.println("    " + description);
            }

            if (filename != null) {
                printWriter.println(FILENAME);
                printWriter.println("    " + filename);
            }

            if (secretKey != null) {
                printWriter.println(SECRET_KEY);
                printWriter.println("    " + secretKey);
            }

            if (initVector != null) {
                printWriter.println(INIT_VECTOR);
                printWriter.println("    " + initVector);
            }

            if (modulus != null) {
                printWriter.println(MODULUS);
                printWriter.println("    " + modulus);
            }

            if (publicExponent != null) {
                printWriter.println(PUBLIC_EXPONENT);
                printWriter.println("    " + publicExponent);
            }

            if (privateExponent != null) {
                printWriter.println(PRIVATE_EXPONENT);
                printWriter.println("    " + privateExponent);
            }

            if (signature != null) {
                printWriter.println(SIGNATURE);
                printWriter.println("    " + signature);
            }

            if (data != null) {
                printWriter.println(DATA);
                printWriter.println("    " + data);
            }

            if (envelopeData != null) {
                printWriter.println(ENVELOPE_DATA);
                printWriter.println("    " + envelopeData);
            }

            if (envelopeCryptKey != null) {
                printWriter.println(ENVELOPE_CRYPT_KEY);
                printWriter.println("    " + envelopeCryptKey);
            }

            if (method != null) {
                printWriter.println(METHOD);

                for (Encryption.EncryptionMethod m : method) {
                    printWriter.println("    " + m.getName());
                }
            }

            if (keyLength != null) {
                printWriter.println(KEY_LENGTH);

                for (String l : keyLength) {
                    printWriter.println("    " + l);
                }
            }

            printWriter.println(CRYPTO_DATA_END);
            printWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Encryption.EncryptionMethod[] getMethod() {
        return method;
    }

    public void setMethod(Encryption.EncryptionMethod[] method) {
        this.method = method;
    }

    public String[] getKeyLength() {
        return keyLength;
    }

    public void setKeyLength(String[] keyLength) {
        this.keyLength = keyLength;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getInitVector() {
        return initVector;
    }

    public void setInitVector(String initVector) {
        this.initVector = initVector;
    }

    public String getModulus() {
        return modulus;
    }

    public void setModulus(String modulus) {
        this.modulus = modulus;
    }

    public String getPublicExponent() {
        return publicExponent;
    }

    public void setPublicExponent(String publicExponent) {
        this.publicExponent = publicExponent;
    }

    public String getPrivateExponent() {
        return privateExponent;
    }

    public void setPrivateExponent(String privateExponent) {
        this.privateExponent = privateExponent;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEnvelopeData() {
        return envelopeData;
    }

    public void setEnvelopeData(String envelopeData) {
        this.envelopeData = envelopeData;
    }

    public String getEnvelopeCryptKey() {
        return envelopeCryptKey;
    }

    public void setEnvelopeCryptKey(String envelopeCryptKey) {
        this.envelopeCryptKey = envelopeCryptKey;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "Content{" +
                "description='" + description + '\'' +
                ", filename='" + filename + '\'' +
                ", method=" + Arrays.toString(method) +
                ", keyLength=" + Arrays.toString(keyLength) +
                ", secretKey='" + secretKey + '\'' +
                ", initVector='" + initVector + '\'' +
                ", modulus='" + modulus + '\'' +
                ", publicExponent='" + publicExponent + '\'' +
                ", privateExponent='" + privateExponent + '\'' +
                ", signature='" + signature + '\'' +
                ", data='" + data + '\'' +
                ", envelopeData='" + envelopeData + '\'' +
                ", envelopeCryptKey='" + envelopeCryptKey + '\'' +
                '}';
    }

    private interface Strategy {
        void setContent(Content content, String value);
    }
}

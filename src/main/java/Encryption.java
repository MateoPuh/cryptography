import org.bouncycastle.jcajce.provider.digest.SHA3;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Encryption {

    public interface SymmetricEncryption {
        String encrypt(String text, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException;

        String decrypt(String encrypted, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException;
    }

    public static class AES implements SymmetricEncryption {

        public String encrypt(String text, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/" + method + "/PKCS5Padding");

            if (method == SymmetricMethod.ECB) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            } else {
                IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            }

            return Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes()));
        }

        public String decrypt(String encrypted, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "AES");

            Cipher cipherDecrypt = Cipher.getInstance("AES/" + method + "/PKCS5Padding");
            if (method == SymmetricMethod.ECB) {
                cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec);
            } else {
                IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
                cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            }

            return new String(cipherDecrypt.doFinal(Base64.getDecoder().decode(encrypted)));
        }
    }

    public static class DES3 implements SymmetricEncryption {

        public String encrypt(String text, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
            System.out.println(key.length());
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "DESede");

            Cipher cipher = Cipher.getInstance("DESede/" + method + "/PKCS5Padding");

            if (method == SymmetricMethod.ECB) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            } else {
                IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
            }

            return Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes()));
        }

        public String decrypt(String encrypted, String key, String iv, SymmetricMethod method) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "DESede");

            Cipher cipherDecrypt = Cipher.getInstance("DESede/" + method + "/PKCS5Padding");
            if (method == SymmetricMethod.ECB) {
                cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec);
            } else {
                IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
                cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            }

            return new String(cipherDecrypt.doFinal(Base64.getDecoder().decode(encrypted)));
        }
    }

    public interface AsymmetricEncryption {
        String encrypt(String plainText, Key publicKey) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException;

        String decrypt(String cipherText, Key privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException;
    }

    public static class RSA implements AsymmetricEncryption {

        public String encrypt(String plainText, Key publicKey) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
            Cipher encryptCipher = Cipher.getInstance("RSA");
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] cipherText = encryptCipher.doFinal(plainText.getBytes(UTF_8));

            return Base64.getEncoder().encodeToString(cipherText);
        }

        public String decrypt(String cipherText, Key privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
            byte[] bytes = Base64.getDecoder().decode(cipherText);

            Cipher decriptCipher = Cipher.getInstance("RSA");
            decriptCipher.init(Cipher.DECRYPT_MODE, privateKey);

            return new String(decriptCipher.doFinal(bytes), UTF_8);
        }
    }

    public interface HashFunction {
        String hash(String text) throws NoSuchAlgorithmException;
        String hash(List<String> text) throws NoSuchAlgorithmException;
    }

    public static class SHA_256 implements HashFunction {
        @Override
        public String hash(String text) throws NoSuchAlgorithmException {
            return Utils.bytesToHex(MessageDigest.getInstance("SHA-256").digest(text.getBytes(UTF_8)));
        }

        @Override
        public String hash(List<String> text) throws NoSuchAlgorithmException {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            for (String line : text) {
                digest.update(line.getBytes(UTF_8));
            }

            return Utils.bytesToHex(digest.digest());
        }
    }

    public static class SHA_512 implements HashFunction {
        @Override
        public String hash(String text) throws NoSuchAlgorithmException {
            return Utils.bytesToHex(MessageDigest.getInstance("SHA-512").digest(text.getBytes(UTF_8)));
        }

        @Override
        public String hash(List<String> text) throws NoSuchAlgorithmException {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");

            for (String line : text) {
                digest.update(line.getBytes(UTF_8));
            }

            return Utils.bytesToHex(digest.digest());
        }
    }

    public static class SHA3_256 implements HashFunction {
        @Override
        public String hash(String text) throws NoSuchAlgorithmException {
            return Utils.bytesToHex(new SHA3.Digest256().digest(text.getBytes(UTF_8)));
        }

        @Override
        public String hash(List<String> text) throws NoSuchAlgorithmException {
            MessageDigest digest = new SHA3.Digest256();

            for (String line : text) {
                digest.update(line.getBytes(UTF_8));
            }

            return Utils.bytesToHex(digest.digest());
        }
    }

    public static class SHA3_512 implements HashFunction {
        @Override
        public String hash(String text) throws NoSuchAlgorithmException {
            return Utils.bytesToHex(new SHA3.Digest512().digest(text.getBytes(UTF_8)));
        }

        @Override
        public String hash(List<String> text) throws NoSuchAlgorithmException {
            MessageDigest digest = new SHA3.Digest512();

            for (String line : text) {
                digest.update(line.getBytes(UTF_8));
            }

            return Utils.bytesToHex(digest.digest());
        }
    }

    public static KeyPair generateKeyPair(int keySize) throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(keySize, new SecureRandom());
        return generator.generateKeyPair();
    }

    public static String sign(String message, PrivateKey privateKey, AsymmetricEncryption encryption, HashFunction hash) throws Exception {
        return encryption.encrypt(hash.hash(message), privateKey);
    }

    public static boolean verifySignature(String message, String signature, PublicKey publicKey, AsymmetricEncryption encryption, HashFunction hash) throws Exception {
        String decrypted = encryption.decrypt(signature, publicKey);
        return hash.hash(message).equals(decrypted);
    }

    public static Key generateSymmetricKey(int keySize) throws NoSuchAlgorithmException {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(keySize);
        return generator.generateKey();
    }

    public static Pair encryptEnvelope(String message, PublicKey publicKey, SymmetricEncryption messageEncryption, AsymmetricEncryption keyEncryption) throws Exception {
        Key key = generateSymmetricKey(128);
        String encrypted = messageEncryption.encrypt(message, Base64.getEncoder().encodeToString(key.getEncoded()), null, SymmetricMethod.ECB);
        String encryptedKey = keyEncryption.encrypt(Base64.getEncoder().encodeToString(key.getEncoded()), publicKey);

        return new Pair(encrypted, encryptedKey);
    }

    public static String decryptEnvelope(String envelopeData, String envelopeKey, PrivateKey privateKey, SymmetricEncryption messageEncryption, AsymmetricEncryption keyEncryption) throws Exception {
        String key = keyEncryption.decrypt(envelopeKey, privateKey);
        return messageEncryption.decrypt(envelopeData, key, null, SymmetricMethod.ECB);
    }

    public static String encryptStamp(String envelope, PrivateKey privateKey, AsymmetricEncryption encryption, HashFunction hash) throws Exception {
        return sign(envelope, privateKey, encryption, hash);
    }

    public enum SymmetricMethod {
        ECB("ECB"),
        CBC("CBC");

        private String name;

        SymmetricMethod(String name) {
            this.name = name;
        }

        public static Optional<SymmetricMethod> fromName(String name) {
            return Arrays.stream(SymmetricMethod.values()).filter(it -> it.name.equals(name)).findFirst();
        }
    }

    public static enum EncryptionMethod {
        AES("AES"),
        DES("DES"),
        RSA("RSA"),
        SHA256("SHA-256"),
        SHA512("SHA-512"),
        SHA3_256("SHA3-256"),
        SHA3_512("SHA3-512");

        private String name;

        EncryptionMethod(String name) {
            this.name = name;
        }

        public static Optional<EncryptionMethod> fromName(String name) {
            return Arrays.stream(EncryptionMethod.values()).filter(it -> it.name.equals(name)).findFirst();
        }

        public String getName() {
            return name;
        }
    }

    public static HashFunction getHashFunction(EncryptionMethod method) {
        switch (method) {
            case SHA256:
                return new SHA_256();
            case SHA512:
                return new SHA_512();
            case SHA3_256:
                return new SHA3_256();
            case SHA3_512:
                return new SHA3_512();
            default:
                throw new IllegalArgumentException();
        }
    }

    public static SymmetricEncryption getSymmetricEncryption(EncryptionMethod method) {
        switch (method) {
            case AES:
                return new AES();
            case DES:
                return new DES3();
            default:
                throw new IllegalArgumentException();
        }
    }

    public static AsymmetricEncryption getAsymmetricEncryption(EncryptionMethod method) {
        switch (method) {
            case RSA:
                return new RSA();
            default:
                throw new IllegalArgumentException();
        }
    }

    public static class Pair {
        private String value1;
        private String value2;

        public Pair(String value1, String value2) {
            this.value1 = value1;
            this.value2 = value2;
        }

        public String getValue1() {
            return value1;
        }

        public void setValue1(String value1) {
            this.value1 = value1;
        }

        public String getValue2() {
            return value2;
        }

        public void setValue2(String value2) {
            this.value2 = value2;
        }
    }
}

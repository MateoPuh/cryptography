import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.*;

public class EncryptionGUI extends JFrame {

    private static final String INPUT_FILE = "Input file";
    private static final String INPUT_KEY = "Key";
    private static final String PRIVATE_KEY = "Private key";
    private static final String PUBLIC_KEY = "Public key";
    private static final String SIGNATURE = "Signature";
    private static final String STAMP = "Stamp";
    private static final String ENVELOPE = "Envelope";

    private Mode currentMode;
    private Encryption.SymmetricMethod symmetricMethod = Encryption.SymmetricMethod.ECB;
    private Map<String, Content> contentMap = new HashMap<>();
    private JPanel mainPanel = new JPanel();
    private JPanel contentPanel = new JPanel();
    private List<JToggleButton> buttons = new ArrayList<>();
    private JTextPane infoPane;

    public EncryptionGUI() {
        setTitle("Encryption");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        initGUI();
        pack();
    }

    public void initGUI() {
        mainPanel = new JPanel();
        add(mainPanel);
        initPanel();
    }

    private void initPanel() {
        mainPanel.setLayout(null);

        JButton runButton = new JButton("Run");
        runButton.setBounds(600, 5, 100, 25);
        mainPanel.add(runButton);
        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (currentMode) {
                    case HASH:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        Content input = contentMap.get(INPUT_FILE);

                        try {
                            Encryption.HashFunction hash = Encryption.getHashFunction(input.getMethod()[0]);

                            Content hashed = new Content();
                            hashed.setDescription("Hash");
                            hashed.setMethod(new Encryption.EncryptionMethod[]{input.getMethod()[0]});
                            hashed.setData(hash.hash(input.getData()));
                            hashed.setFilename(input.getFilename());

                            Path inputPath = Paths.get(input.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-hash";

                            hashed.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Hash succesfully generated!\nSaved in: " + outputPath + "\n" + hash.hash(input.getData()));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case SYMMETRIC_ENCRYPT:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(INPUT_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select key!");
                        }

                        Content encryptInput = contentMap.get(INPUT_FILE);
                        Content encryptKey = contentMap.get(INPUT_KEY);

                        try {
                            Encryption.SymmetricEncryption encryption = Encryption.getSymmetricEncryption(encryptInput.getMethod()[0]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Encrypted file");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{encryptInput.getMethod()[0]});
                            encrypted.setData(encryption.encrypt(encryptInput.getData(), encryptKey.getSecretKey(), encryptInput.getInitVector(), symmetricMethod));
                            encrypted.setFilename(encryptInput.getFilename());

                            Path inputPath = Paths.get(encryptInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-encrypted";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Encryption successful!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case SYMMETRIC_DECRYPT:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(INPUT_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select key!");
                        }

                        Content decryptInput = contentMap.get(INPUT_FILE);
                        Content decryptKey = contentMap.get(INPUT_KEY);

                        try {
                            Encryption.SymmetricEncryption encryption = Encryption.getSymmetricEncryption(decryptInput.getMethod()[0]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Decrypted file");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{decryptInput.getMethod()[0]});
                            encrypted.setData(encryption.decrypt(decryptInput.getData(), decryptKey.getSecretKey(), decryptInput.getInitVector(), symmetricMethod));
                            encrypted.setFilename(decryptInput.getFilename());

                            Path inputPath = Paths.get(decryptInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-decrypted";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Decryption successful!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case ASYMMETRIC_ENCRYPT:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PUBLIC_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select public key!");
                        }

                        Content asymEncryptInput = contentMap.get(INPUT_FILE);
                        Content asymEncryptKey = contentMap.get(PUBLIC_KEY);

                        try {
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(asymEncryptInput.getMethod()[0]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Encrypted file");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{asymEncryptInput.getMethod()[0]});
                            EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(asymEncryptKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PublicKey k = keyFactory.generatePublic(publicSpec);
                            encrypted.setData(encryption.encrypt(asymEncryptInput.getData(), k));
                            encrypted.setFilename(asymEncryptInput.getFilename());

                            Path inputPath = Paths.get(asymEncryptInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-encrypted";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Encryption successful!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case ASYMMETRIC_DECRYPT:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PRIVATE_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select private key!");
                        }

                        Content asymDecryptInput = contentMap.get(INPUT_FILE);
                        Content asymDecryptKey = contentMap.get(PRIVATE_KEY);

                        try {
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(asymDecryptInput.getMethod()[0]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Decrypted file");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{asymDecryptInput.getMethod()[0]});
                            EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(asymDecryptKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PrivateKey k = keyFactory.generatePrivate(privateSpec);
                            encrypted.setData(encryption.decrypt(asymDecryptInput.getData(), k));
                            encrypted.setFilename(asymDecryptInput.getFilename());

                            Path inputPath = Paths.get(asymDecryptInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-decrypted";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Decryption successful!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case SIGN:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PRIVATE_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select private key!");
                        }

                        Content signInput = contentMap.get(INPUT_FILE);
                        Content signKey = contentMap.get(PRIVATE_KEY);

                        try {
                            Encryption.HashFunction hash = Encryption.getHashFunction(signInput.getMethod()[0]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(signInput.getMethod()[1]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Signature");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{signInput.getMethod()[0]});
                            EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(signKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PrivateKey k = keyFactory.generatePrivate(privateSpec);
                            encrypted.setData(Encryption.sign(signInput.getData(), k, encryption, hash));
                            encrypted.setFilename(signInput.getFilename());

                            Path inputPath = Paths.get(signInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-signature";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Signature successfully created!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case VERIFY:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PUBLIC_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select public key!");
                        }

                        if (!contentMap.containsKey(SIGNATURE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select signature!");
                        }

                        Content verifyInput = contentMap.get(INPUT_FILE);
                        Content verifyKey = contentMap.get(PUBLIC_KEY);
                        Content verifySignature = contentMap.get(SIGNATURE);

                        try {
                            Encryption.HashFunction hash = Encryption.getHashFunction(verifyInput.getMethod()[0]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(verifyInput.getMethod()[1]);


                            EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(verifyKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PublicKey k = keyFactory.generatePublic(publicSpec);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText(Encryption.verifySignature(verifyInput.getData(), verifySignature.getData(), k, encryption, hash) ? "Signature verified!" : "Signature doesn't match!");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case ENVELOPE:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PUBLIC_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select public key!");
                        }

                        Content envInput = contentMap.get(INPUT_FILE);
                        Content envKey = contentMap.get(PUBLIC_KEY);

                        try {
                            Encryption.SymmetricEncryption symmetricEncryption = Encryption.getSymmetricEncryption(envInput.getMethod()[0]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(envInput.getMethod()[1]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Envelope");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{envInput.getMethod()[0], envInput.getMethod()[1]});
                            EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(envKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PublicKey k = keyFactory.generatePublic(publicSpec);
                            Encryption.Pair envelope = Encryption.encryptEnvelope(envInput.getData(), k, symmetricEncryption, encryption);
                            encrypted.setEnvelopeData(envelope.getValue1());
                            encrypted.setEnvelopeCryptKey(envelope.getValue2());
                            encrypted.setFilename(envInput.getFilename());

                            Path inputPath = Paths.get(envInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-envelope";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Envelope successfully created!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case DECRYPT_ENVELOPE:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PRIVATE_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select private key!");
                        }

                        Content envdInput = contentMap.get(INPUT_FILE);
                        Content envdKey = contentMap.get(PRIVATE_KEY);

                        try {
                            Encryption.SymmetricEncryption symmetricEncryption = Encryption.getSymmetricEncryption(envdInput.getMethod()[0]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(envdInput.getMethod()[1]);

                            String envelopeData = envdInput.getEnvelopeData();
                            String envelopeKey = envdInput.getEnvelopeCryptKey();

                            Content encrypted = new Content();
                            encrypted.setDescription("Decrypted Envelope");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{envdInput.getMethod()[0], envdInput.getMethod()[1]});
                            EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(envdKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PrivateKey k = keyFactory.generatePrivate(privateSpec);
                            encrypted.setData(Encryption.decryptEnvelope(envelopeData, envelopeKey, k, symmetricEncryption, encryption));
                            encrypted.setFilename(envdInput.getFilename());

                            Path inputPath = Paths.get(envdInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-decrypted";

                            encrypted.toFile(outputPath);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Envelope successfully decrypted!\nSaved in " + outputPath + "\n");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!\n" + ex.getLocalizedMessage());
                        }
                        break;
                    case STAMP:
                        if (!contentMap.containsKey(INPUT_FILE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PUBLIC_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select public key!");
                        }

                        if (!contentMap.containsKey(PRIVATE_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select private key!");
                        }

                        Content stampInput = contentMap.get(INPUT_FILE);
                        Content stampKey = contentMap.get(PUBLIC_KEY);
                        Content stampPrivateKey = contentMap.get(PRIVATE_KEY);

                        try {
                            Encryption.HashFunction hash = Encryption.getHashFunction(stampInput.getMethod()[0]);
                            Encryption.SymmetricEncryption symmetricEncryption = Encryption.getSymmetricEncryption(stampInput.getMethod()[1]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(stampInput.getMethod()[2]);

                            Content encrypted = new Content();
                            encrypted.setDescription("Stamp");
                            encrypted.setMethod(new Encryption.EncryptionMethod[]{stampInput.getMethod()[0], stampInput.getMethod()[1], stampInput.getMethod()[2]});

                            EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(stampKey.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PublicKey k = keyFactory.generatePublic(publicSpec);

                            EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(stampPrivateKey.getSecretKey()));
                            PrivateKey privateK = keyFactory.generatePrivate(privateSpec);

                            Encryption.Pair envelope = Encryption.encryptEnvelope(stampInput.getData(), k, symmetricEncryption, encryption);

                            encrypted.setData(Encryption.encryptStamp(envelope.getValue1() + envelope.getValue2(), privateK, encryption, hash));
                            encrypted.setFilename(stampInput.getFilename());

                            Path inputPath = Paths.get(stampInput.getFilePath());
                            String outputPath = inputPath.getParent().toString() + "/" + inputPath.getFileName() + "-stamp";

                            encrypted.toFile(outputPath);

                            Content env = new Content();
                            env.setDescription("Envelope");
                            env.setMethod(new Encryption.EncryptionMethod[]{stampInput.getMethod()[1], stampInput.getMethod()[2]});
                            env.setEnvelopeData(envelope.getValue1());
                            env.setEnvelopeCryptKey(envelope.getValue2());

                            Path inputPathEnv = Paths.get(stampInput.getFilePath());
                            String outputPathEnv = inputPathEnv.getParent().toString() + "/" + inputPathEnv.getFileName() + "-envelope";

                            env.toFile(outputPathEnv);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText("Stamp created successfully!\nSaved in " + outputPath + "\n" + "Envelope saved in" + outputPathEnv);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!");
                        }
                        break;
                    case DECRYPT_STAMP:
                        if (!contentMap.containsKey(STAMP)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(ENVELOPE)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select input file!");
                        }

                        if (!contentMap.containsKey(PUBLIC_KEY)) {
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Please select key!");
                        }

                        Content stamp = contentMap.get(STAMP);
                        Content env = contentMap.get(ENVELOPE);
                        Content stampPublic = contentMap.get(PUBLIC_KEY);

                        try {
                            Encryption.HashFunction hash = Encryption.getHashFunction(stamp.getMethod()[0]);
                            Encryption.AsymmetricEncryption encryption = Encryption.getAsymmetricEncryption(stamp.getMethod()[2]);

                            EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.getDecoder().decode(stampPublic.getSecretKey()));
                            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                            PublicKey k = keyFactory.generatePublic(publicSpec);

                            infoPane.setForeground(Color.LIGHT_GRAY);
                            infoPane.setText(Encryption.verifySignature(env.getEnvelopeData() + env.getEnvelopeCryptKey(), stamp.getData(), k, encryption, hash) ? "Stamp verified!" : "Stamo doesn't match");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            infoPane.setForeground(Color.RED);
                            infoPane.setText("Error occured! Please check your file!");
                        }
                        break;
                }
            }
        });


        JComboBox<Option> optionList = new JComboBox<>(Option.values());
        optionList.setSelectedIndex(-1);
        optionList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removePanel(mainPanel);
                addNewPanel(mainPanel);

                int y = 5;

                JComboBox<Option> cb = (JComboBox<Option>) (e.getSource());
                switch ((Option) cb.getSelectedItem()) {
                    case SYMMETRIC:
                        currentMode = Mode.SYMMETRIC_ENCRYPT;
                        removeButtons();
                        setSymmetricEncryptionButtons();
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        addInputField(contentPanel, INPUT_KEY, y);
                        y += 35;
                        break;
                    case ASYMMETRIC:
                        currentMode = Mode.ASYMMETRIC_ENCRYPT;
                        removeButtons();
                        setAsymmetricEncryptionButtons();
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        addInputField(contentPanel, PUBLIC_KEY, y);
                        y += 35;
                        addInputField(contentPanel, PRIVATE_KEY, y);
                        y += 35;
                        break;
                    case HASH:
                        currentMode = Mode.HASH;
                        removeButtons();
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        break;
                    case SIGN:
                        removeButtons();
                        setSignButtons();
                        currentMode = Mode.SIGN;
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        addInputField(contentPanel, PUBLIC_KEY, y);
                        y += 35;
                        addInputField(contentPanel, PRIVATE_KEY, y);
                        y += 35;
                        addInputField(contentPanel, SIGNATURE, y);
                        y += 35;
                        break;
                    case ENVELOPE:
                        removeButtons();
                        setEnvelopeButtons();
                        currentMode = Mode.ENVELOPE;
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        addInputField(contentPanel, PUBLIC_KEY, y);
                        y += 35;
                        addInputField(contentPanel, PRIVATE_KEY, y);
                        y += 35;
                        break;
                    case STAMP:
                        removeButtons();
                        setStampButtons();
                        currentMode = Mode.STAMP;
                        addInputField(contentPanel, INPUT_FILE, y);
                        y += 35;
                        addInputField(contentPanel, STAMP, y);
                        y += 35;
                        addInputField(contentPanel, ENVELOPE, y);
                        y += 35;
                        addInputField(contentPanel, PUBLIC_KEY, y);
                        y += 35;
                        addInputField(contentPanel, PRIVATE_KEY, y);
                        y += 35;
                        break;
                }

                y += 15;
                infoPane = addTextPane(contentPanel, y);
                infoPane.setForeground(Color.RED);
                infoPane.setBackground(Color.BLACK);
                infoPane.setEditable(false);
            }
        });
        optionList.setBounds(5, 5, 200, 25);
        mainPanel.add(optionList);
    }

    private void setKeyPairGenerator() {
        JToggleButton encryptButton = new JToggleButton("Create keys");
        encryptButton.setBounds(475, 5, 100, 25);
        mainPanel.add(encryptButton);
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String curDir = System.getProperty("user.dir");

                try {
                    KeyPair keyPair = Encryption.generateKeyPair(2048);
                    Content privateKey = new Content();
                    privateKey.setDescription("Private key");
                    privateKey.setMethod(new Encryption.EncryptionMethod[] {Encryption.EncryptionMethod.RSA});
                    privateKey.setSecretKey(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));

                    Content publicKey = new Content();
                    publicKey.setDescription("Public key");
                    publicKey.setMethod(new Encryption.EncryptionMethod[] {Encryption.EncryptionMethod.RSA});
                    publicKey.setSecretKey(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));

                    privateKey.toFile(curDir + "/" + "privatekey");
                    publicKey.toFile(curDir + "/" + "publickey");

                    infoPane.setForeground(Color.LIGHT_GRAY);
                    infoPane.setText("Generated key pair:\n" + "private key: " + curDir + "/" + "privatekey" + "\n" +  "public key: " + curDir + "/" + "publickey");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                encryptButton.setEnabled(false);
            }
        });
        buttons.add(encryptButton);
    }

    private void setAsymmetricEncryptionButtons() {
        setEncryptionButtons(Mode.ASYMMETRIC_ENCRYPT, Mode.ASYMMETRIC_DECRYPT);
        setKeyPairGenerator();
    }

    private void setSymmetricEncryptionButtons() {
        setEncryptionButtons(Mode.SYMMETRIC_ENCRYPT, Mode.SYMMETRIC_DECRYPT);

        JToggleButton encryptButton = new JToggleButton("EBC");
        JToggleButton decryptButton = new JToggleButton("CBC");

        encryptButton.setBounds(475, 5, 50, 25);
        mainPanel.add(encryptButton);
        encryptButton.setSelected(true);
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                symmetricMethod = Encryption.SymmetricMethod.ECB;
                encryptButton.setSelected(true);
                decryptButton.setSelected(false);
            }
        });

        decryptButton.setBounds(525, 5, 50, 25);
        mainPanel.add(decryptButton);
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                symmetricMethod = Encryption.SymmetricMethod.CBC;
                encryptButton.setSelected(false);
                decryptButton.setSelected(true);
            }
        });

        buttons.add(encryptButton);
        buttons.add(decryptButton);
    }

    private void setEncryptionButtons(Mode mode1, Mode mode2) {
        JToggleButton encryptButton = new JToggleButton("Encryption");
        JToggleButton decryptButton = new JToggleButton("Decryption");

        encryptButton.setBounds(250, 5, 100, 25);
        mainPanel.add(encryptButton);
        encryptButton.setSelected(true);
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = mode1;
                encryptButton.setSelected(true);
                decryptButton.setSelected(false);
            }
        });

        decryptButton.setBounds(350, 5, 100, 25);
        mainPanel.add(decryptButton);
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = mode2;
                encryptButton.setSelected(false);
                decryptButton.setSelected(true);
            }
        });

        buttons = new ArrayList<>();
        buttons.add(encryptButton);
        buttons.add(decryptButton);
    }

    private void setSignButtons() {
        JToggleButton signButton = new JToggleButton("Sign");
        JToggleButton verifyButton = new JToggleButton("Verify");

        signButton.setBounds(250, 5, 100, 25);
        mainPanel.add(signButton);
        signButton.setSelected(true);
        signButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.SIGN;
                signButton.setSelected(true);
                verifyButton.setSelected(false);
            }
        });

        verifyButton.setBounds(350, 5, 100, 25);
        mainPanel.add(verifyButton);
        verifyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.VERIFY;
                signButton.setSelected(false);
                verifyButton.setSelected(true);
            }
        });

        buttons = new ArrayList<>();
        buttons.add(signButton);
        buttons.add(verifyButton);
    }

    private void setEnvelopeButtons() {
        JToggleButton envelopeButton = new JToggleButton("Envelope");
        JToggleButton decryptButton = new JToggleButton("Decrypt");

        envelopeButton.setBounds(250, 5, 100, 25);
        mainPanel.add(envelopeButton);
        envelopeButton.setSelected(true);
        envelopeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.ENVELOPE;
                envelopeButton.setSelected(true);
                decryptButton.setSelected(false);
            }
        });

        decryptButton.setBounds(350, 5, 100, 25);
        mainPanel.add(decryptButton);
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.DECRYPT_ENVELOPE;
                envelopeButton.setSelected(false);
                decryptButton.setSelected(true);
            }
        });

        buttons = new ArrayList<>();
        buttons.add(envelopeButton);
        buttons.add(decryptButton);
    }

    private void setStampButtons() {
        JToggleButton stampButton = new JToggleButton("Stamp");
        JToggleButton decryptButton = new JToggleButton("Verify");

        stampButton.setBounds(250, 5, 100, 25);
        mainPanel.add(stampButton);
        stampButton.setSelected(true);
        stampButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.STAMP;
                stampButton.setSelected(true);
                decryptButton.setSelected(false);
            }
        });

        decryptButton.setBounds(350, 5, 100, 25);
        mainPanel.add(decryptButton);
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentMode = Mode.DECRYPT_STAMP;
                stampButton.setSelected(false);
                decryptButton.setSelected(true);
            }
        });

        buttons = new ArrayList<>();
        buttons.add(stampButton);
        buttons.add(decryptButton);
    }

    private void removeButtons() {
        for (JToggleButton button : buttons) {
            mainPanel.remove(button);
        }
        revalidate();
        repaint();
        buttons = new ArrayList<>();
    }

    private void addNewPanel(JPanel panel) {
        JPanel p = new JPanel();
        p.setBounds(30, 30, 500, 500);
        p.setLayout(null);
        panel.add(p);
        contentPanel = p;
    }

    private void removePanel(JPanel panel) {
        panel.remove(contentPanel);
        revalidate();
        repaint();
    }

    private void addInputField(JPanel panel, String name, int y) {
        JLabel inputLabel = new JLabel(name + ":");

        inputLabel.setBounds(5, y, 80, 25);
        panel.add(inputLabel);

        JTextField inputTextField = new JTextField(20);
        inputTextField.setBounds(100, y, 300, 25);
        inputTextField.setEditable(false);
        panel.add(inputTextField);

        JButton openFileButton = new JButton("Open");
        openFileButton.setBounds(415, y, 100, 25);
        panel.add(openFileButton);
        openFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String curDir = System.getProperty("user.dir");

                JFileChooser jfc = new JFileChooser(curDir);
                jfc.setDialogTitle("Choose an input file: ");

                if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = jfc.getSelectedFile();
                    inputTextField.setText(selectedFile.getAbsolutePath());
                    try {
                        Content content = Content.fromFile(selectedFile.getAbsolutePath());
                        contentMap.put(name, content);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private JTextPane addTextPane(JPanel panel, int y) {
        JTextPane label = new JTextPane();

        label.setBounds(5, y, 500, 150);
        panel.add(label);

        return label;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                EncryptionGUI m = new EncryptionGUI();
                m.setVisible(true);
                m.setSize(700, 400);
            }
        });
    }

    private static void placeComponents(JPanel panel) {
        // Creating login button
        JButton loginButton = new JButton("login");
        loginButton.setBounds(10, 80, 80, 25);
        panel.add(loginButton);
    }

    private enum Option {
        SYMMETRIC("Symmetric"),
        ASYMMETRIC("Asymmetric"),
        HASH("Hash"),
        SIGN("Sign"),
        ENVELOPE("Envelope"),
        STAMP("Stamp");

        private String name;

        Option(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        @Override
        public String toString() {
            return name;
        }
    }

    private enum Mode {
        SYMMETRIC_ENCRYPT,
        SYMMETRIC_DECRYPT,
        ASYMMETRIC_ENCRYPT,
        ASYMMETRIC_DECRYPT,
        HASH,
        SIGN,
        VERIFY,
        ENVELOPE,
        DECRYPT_ENVELOPE,
        STAMP,
        DECRYPT_STAMP
    }


}